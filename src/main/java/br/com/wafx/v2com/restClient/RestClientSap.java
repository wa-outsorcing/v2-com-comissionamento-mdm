package br.com.wafx.v2com.restClient;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

/**
 * @author Kaio Maximiano
 */

@Path("/consumer-unit")
@RegisterRestClient
public interface RestClientSap {

	@GET
	@Path("/installations")
	@Produces(MediaType.APPLICATION_JSON)
	String findInstalations();
	
}

