package br.com.wafx.v2com.restClient;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

/**
 * @author Kaio Maximiano
 */

@Path("/mdm-vision")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@RegisterRestClient
public interface RestClientMdmVision {
		
	@GET
    @Path("/commissioning/non-billables")
	String findInstalationsNonBillable();
	
	@PATCH
    @Path("/commissioning/{meterId}/{installationCode}")
	String updateRegisterInstalation(@PathParam("meterId") String meterId, @PathParam("installationCode") String installationCode);

	@GET
    @Path("/installations-cache/installation/{meterId}/{installationCode}")
	String findInstalationCache(@PathParam("meterId") String meterId, @PathParam("installationCode") String installationCode);

	@POST
	@Path("/commission-installations")
	String commissionInstalation();
	
	@PUT
	@Path("/commissioning/{meterId}/{installationCode}")
	String commissioningInstalation(@PathParam("meterId") String meterId, @PathParam("installationCode") String installationCode);
	
}
