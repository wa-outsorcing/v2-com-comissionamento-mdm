package br.com.wafx.v2com.model;

/**
 * @author Kaio Maximiano
 */
public class Installation {
	
	public Installation() {}

	public Installation(String meterId, String installationCode, Integer number, String name, String lastName,
			String peeCode, String ccsClassActivity, String supplyReason, Integer streetCode, String streetName,
			Integer portNumber, String block, String complement1, String complement2, String portComplement,
			Integer floor, String room, String districtName, String cityName, Integer cityId, Boolean irrigating,
			String status, Integer solicitationNumber, String taxGroupType, String activitySubclass, String cnae,
			Boolean telemetry, String phasing, String concentrator, String peeXCoordinate, String peeYCoordinate,
			String primaryFeeder, String secondaryFeeder, String substation, String boundaryZone,
			String patrimonyTransformerEquipment, String transformerPhases, String transformerPower, String type,
			Double dailyAverageConsumption) {
		this.meterId = meterId;
		this.installationCode = installationCode;
		this.number = number;
		this.name = name;
		this.lastName = lastName;
		this.peeCode = peeCode;
		this.ccsClassActivity = ccsClassActivity;
		this.supplyReason = supplyReason;
		this.streetCode = streetCode;
		this.streetName = streetName;
		this.portNumber = portNumber;
		this.block = block;
		this.complement1 = complement1;
		this.complement2 = complement2;
		this.portComplement = portComplement;
		this.floor = floor;
		this.room = room;
		this.districtName = districtName;
		this.cityName = cityName;
		this.cityId = cityId;
		this.irrigating = irrigating;
		this.status = status;
		this.solicitationNumber = solicitationNumber;
		this.taxGroupType = taxGroupType;
		this.activitySubclass = activitySubclass;
		this.cnae = cnae;
		this.telemetry = telemetry;
		this.phasing = phasing;
		this.concentrator = concentrator;
		this.peeXCoordinate = peeXCoordinate;
		this.peeYCoordinate = peeYCoordinate;
		this.primaryFeeder = primaryFeeder;
		this.secondaryFeeder = secondaryFeeder;
		this.substation = substation;
		this.boundaryZone = boundaryZone;
		this.patrimonyTransformerEquipment = patrimonyTransformerEquipment;
		this.transformerPhases = transformerPhases;
		this.transformerPower = transformerPower;
		this.type = type;
		this.dailyAverageConsumption = dailyAverageConsumption;
	}

	private String meterId;
	private String installationCode;
	private Integer number;
	private String name;
	private String lastName;
	private String peeCode;
	private String ccsClassActivity;
	private String supplyReason;
	private Integer streetCode;
	private String streetName;
	private Integer portNumber;
	private String block;
	private String complement1;
	private String complement2;
	private String portComplement;
	private Integer floor;
	private String room;
	private String districtName;
	private String cityName;
	private Integer cityId;
	private Boolean irrigating;
	private String status;
	private Integer solicitationNumber;
	private String taxGroupType;
	private String activitySubclass;
	private String cnae;
	private Boolean telemetry;
	private String phasing;
	private String concentrator;
	private String peeXCoordinate;
	private String peeYCoordinate;
	private String primaryFeeder;
	private String secondaryFeeder;
	private String substation;
	private String boundaryZone;
	private String patrimonyTransformerEquipment;
	private String transformerPhases;
	private String transformerPower;
	private String type;
	private Double dailyAverageConsumption;

	public String getMeterId() {
		return meterId;
	}

	public void setMeterId(String meterId) {
		this.meterId = meterId;
	}

	public String getInstallationCode() {
		return installationCode;
	}

	public void setInstallationCode(String installationCode) {
		this.installationCode = installationCode;
	}

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPeeCode() {
		return peeCode;
	}

	public void setPeeCode(String peeCode) {
		this.peeCode = peeCode;
	}

	public String getCcsClassActivity() {
		return ccsClassActivity;
	}

	public void setCcsClassActivity(String ccsClassActivity) {
		this.ccsClassActivity = ccsClassActivity;
	}

	public String getSupplyReason() {
		return supplyReason;
	}

	public void setSupplyReason(String supplyReason) {
		this.supplyReason = supplyReason;
	}

	public Integer getStreetCode() {
		return streetCode;
	}

	public void setStreetCode(Integer streetCode) {
		this.streetCode = streetCode;
	}

	public String getStreetName() {
		return streetName;
	}

	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}

	public Integer getPortNumber() {
		return portNumber;
	}

	public void setPortNumber(Integer portNumber) {
		this.portNumber = portNumber;
	}

	public String getBlock() {
		return block;
	}

	public void setBlock(String block) {
		this.block = block;
	}

	public String getComplement1() {
		return complement1;
	}

	public void setComplement1(String complement1) {
		this.complement1 = complement1;
	}

	public String getComplement2() {
		return complement2;
	}

	public void setComplement2(String complement2) {
		this.complement2 = complement2;
	}

	public String getPortComplement() {
		return portComplement;
	}

	public void setPortComplement(String portComplement) {
		this.portComplement = portComplement;
	}

	public Integer getFloor() {
		return floor;
	}

	public void setFloor(Integer floor) {
		this.floor = floor;
	}

	public String getRoom() {
		return room;
	}

	public void setRoom(String room) {
		this.room = room;
	}

	public String getDistrictName() {
		return districtName;
	}

	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public Integer getCityId() {
		return cityId;
	}

	public void setCityId(Integer cityId) {
		this.cityId = cityId;
	}

	public Boolean getIrrigating() {
		return irrigating;
	}

	public void setIrrigating(Boolean irrigating) {
		this.irrigating = irrigating;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getSolicitationNumber() {
		return solicitationNumber;
	}

	public void setSolicitationNumber(Integer solicitationNumber) {
		this.solicitationNumber = solicitationNumber;
	}

	public String getTaxGroupType() {
		return taxGroupType;
	}

	public void setTaxGroupType(String taxGroupType) {
		this.taxGroupType = taxGroupType;
	}

	public String getActivitySubclass() {
		return activitySubclass;
	}

	public void setActivitySubclass(String activitySubclass) {
		this.activitySubclass = activitySubclass;
	}

	public String getCnae() {
		return cnae;
	}

	public void setCnae(String cnae) {
		this.cnae = cnae;
	}

	public Boolean getTelemetry() {
		return telemetry;
	}

	public void setTelemetry(Boolean telemetry) {
		this.telemetry = telemetry;
	}

	public String getPhasing() {
		return phasing;
	}

	public void setPhasing(String phasing) {
		this.phasing = phasing;
	}

	public String getConcentrator() {
		return concentrator;
	}

	public void setConcentrator(String concentrator) {
		this.concentrator = concentrator;
	}

	public String getPeeXCoordinate() {
		return peeXCoordinate;
	}

	public void setPeeXCoordinate(String peeXCoordinate) {
		this.peeXCoordinate = peeXCoordinate;
	}

	public String getPeeYCoordinate() {
		return peeYCoordinate;
	}

	public void setPeeYCoordinate(String peeYCoordinate) {
		this.peeYCoordinate = peeYCoordinate;
	}

	public String getPrimaryFeeder() {
		return primaryFeeder;
	}

	public void setPrimaryFeeder(String primaryFeeder) {
		this.primaryFeeder = primaryFeeder;
	}

	public String getSecondaryFeeder() {
		return secondaryFeeder;
	}

	public void setSecondaryFeeder(String secondaryFeeder) {
		this.secondaryFeeder = secondaryFeeder;
	}

	public String getSubstation() {
		return substation;
	}

	public void setSubstation(String substation) {
		this.substation = substation;
	}

	public String getBoundaryZone() {
		return boundaryZone;
	}

	public void setBoundaryZone(String boundaryZone) {
		this.boundaryZone = boundaryZone;
	}

	public String getPatrimonyTransformerEquipment() {
		return patrimonyTransformerEquipment;
	}

	public void setPatrimonyTransformerEquipment(String patrimonyTransformerEquipment) {
		this.patrimonyTransformerEquipment = patrimonyTransformerEquipment;
	}

	public String getTransformerPhases() {
		return transformerPhases;
	}

	public void setTransformerPhases(String transformerPhases) {
		this.transformerPhases = transformerPhases;
	}

	public String getTransformerPower() {
		return transformerPower;
	}

	public void setTransformerPower(String transformerPower) {
		this.transformerPower = transformerPower;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Double getDailyAverageConsumption() {
		return dailyAverageConsumption;
	}

	public void setDailyAverageConsumption(Double dailyAverageConsumption) {
		this.dailyAverageConsumption = dailyAverageConsumption;
	}

}