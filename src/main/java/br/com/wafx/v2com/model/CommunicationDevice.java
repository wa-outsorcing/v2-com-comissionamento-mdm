package br.com.wafx.v2com.model;
/**
 * @author Kaio Maximiano
 */
public class CommunicationDevice {

	private String key;
	private LifecycleEventType eventType;
	private String identificationCode;
	private String description;
	private String serialNumber;
	private String situationType;
	private String deviceType;
	private Boolean excluded;
	private String version;
	private Integer timestamp;

	public CommunicationDevice() {
	}

	public CommunicationDevice(String key, LifecycleEventType eventType, String identificationCode, String description,
			String serialNumber, String situationType, String deviceType, Boolean excluded, String version,
			Integer timestamp) {
		this.key = key;
		this.eventType = eventType;
		this.identificationCode = identificationCode;
		this.description = description;
		this.serialNumber = serialNumber;
		this.situationType = situationType;
		this.deviceType = deviceType;
		this.excluded = excluded;
		this.version = version;
		this.timestamp = timestamp;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public LifecycleEventType getEventType() {
		return eventType;
	}

	public void setEventType(LifecycleEventType eventType) {
		this.eventType = eventType;
	}

	public String getIdentificationCode() {
		return identificationCode;
	}

	public void setIdentificationCode(String identificationCode) {
		this.identificationCode = identificationCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public String getSituationType() {
		return situationType;
	}

	public void setSituationType(String situationType) {
		this.situationType = situationType;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	public Boolean getExcluded() {
		return excluded;
	}

	public void setExcluded(Boolean excluded) {
		this.excluded = excluded;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public Integer getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Integer timestamp) {
		this.timestamp = timestamp;
	}

}
