package br.com.wafx.v2com.model;

/**
 * @author Kaio Maximiano
 */
public class NonBillable {

	private String meterId;
	private String installationCode;

	public String getMeterId() {
		return meterId;
	}

	public void setMeterId(String meterId) {
		this.meterId = meterId;
	}

	public String getInstallationCode() {
		return installationCode;
	}

	public void setInstallationCode(String installationCode) {
		this.installationCode = installationCode;
	}
	
}
